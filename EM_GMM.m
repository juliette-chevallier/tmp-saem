function [alpha,mu,Sigma,iter,strg,tau] = EM_GMM (Y,alpha0,mu0,Sigma0,maxIter,pres)
    % Y = vector of observations
    % mu0 = vector of initial means
    % Sigma0 = vector of the initial covariance matrices
    % alpha0 = vector of the initial proposals
    % Nburnin = burn-in period
    % maxIter = limit nb of iterations
    % pres = precision

    m = length(alpha0) ;  % nb clusters
    [n,d] = size(Y) ;     % nb,dim observations

    mu = mu0 ; mu_old = 10*mu ;
    Sigma = Sigma0 ;
    alpha = alpha0 ;

    DetSigma = zeros(1,m) ; sm = zeros(n,m) ;
    Ym = zeros(n,d,m) ;
    for j = 1:m
        Ym(:,:,j) = Y-mu(:,j)' ;
    end

    iter = 0 ;
    while (norm(mu-mu_old)>pres) && (iter<maxIter)
        iter = iter+1 ;
        mu_old = mu ;

        for j = 1:m
           sm(:,j) = sum(Ym(:,:,j)/Sigma(:,:,j) .* Ym(:,:,j), 2) ;
           DetSigma(j) = det(Sigma(:,:,j)) ;
        end
        tau = 1./((sqrt(2*pi)^d)*DetSigma) .* exp(-.5*sm) .*alpha ;
        tau = tau./(sum(tau,2)) ;

        stau = sum(tau) ;
        alpha = stau/n ;
        mu = Y'*tau./stau ;
        Sigma = zeros(d,d,m) ;
        for j = 1:m
            Ym(:,:,j) = Y-mu(:,j)' ;
            for i= 1:n
                Sigma(:,:,j) = Sigma(:,:,j) + (tau(i,j).*(Ym(i,:,j)'*Ym(i,:,j))) ;
            end
            Sigma(:,:,j) = Sigma(:,:,j)/stau(j) ;
        end
    end

    if iter >= maxIter
        disp(' ')
        disp('EM::need more iterations')
    else
        strg = ['EM - Cvgce takes ',num2str(iter),' iters'] ;
        disp(' ')
        disp(['EM algorithm : Convergence takes ',num2str(iter),' iterations.'])
        disp(' ')
    end
end
    