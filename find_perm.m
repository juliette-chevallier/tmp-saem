function idx = find_perm(mu,mu_true)

	[~,m] = size(mu_true) ;
	P = perms(1:m) ;
	N = size(P,1) ;

	dist = zeros(1,N) ;
	for i = 1:N
	    p = P(i,:) ;
	    dist(i) = sum(sqrt(sum(mu(:,p)-mu_true))) ;
	end
	[~,i] = min(dist) ;
	idx = P(i,:) ;

end