function opt = tmpSAEM_GMM (Y,alt)

    % alt = 1  - vanilla tmpSAEM
    % alt = 2  - plot tmpSAEM
    % alt = 3  - vec tmpSAEM

    if nargin<2
        alt = 1 ;
    end

    if alt==1
        opt.tmpSAEM = @VANILLA_tempering_SAEM_GMM ;
    elseif alt == 2
        opt.tmpSAEM = @PLOT_tempering_SAEM_GMM ;
    else
        opt.tmpSAEM = @VEC_tempering_SAEM_GMM ;
    end
        
    
    function [alpha,mu,Sigma,iter,strg,vecT,tau] = VANILLA_tempering_SAEM_GMM (alpha0,mu0,Sigma0,Nburnin,maxIter,pres)
        % Y = vector of observations
        % mu0 = vector of initial means
        % Sigma0 = vector of the initial covariance matrices
        % alpha0 = vector of the initial proposals
        % Nburnin = burn-in period
        % maxIter = limit nb of iterations
        % pres = precision

        m = length(alpha0) ;  % nb clusters
        [n,d] = size(Y) ;     % nb,dim observations

        mu = mu0 ; mu_old = 10*mu ;
        Sigma = Sigma0 ;
        alpha = alpha0 ;

        t = 1:maxIter ; tt = (t+tmp.c*tmp.r)/tmp.r ;
        vecT = 1+ tmp.a.^tt + tmp.b*sin(tt)./tt ;
        T = vecT(1) ;

        gamma = [ones(1,Nburnin),1./(1:maxIter-Nburnin).^(.66)] ;
        S1 = zeros(1,m) ;  S2 = zeros(d,m) ;  S3 = zeros(d,d,m) ;

        sm = zeros(n,m) ;  DetSigma = zeros(1,m) ;

        iter = 0 ;
        while (norm(mu-mu_old)>pres) && (iter<maxIter)
            iter = iter+1 ; 
            mu_old = mu ;

            for j = 1:m
                Ym = Y-mu(:,j)' ;
                sm(:,j) = sum(Ym/Sigma(:,:,j) .* Ym, 2) ;
                DetSigma(j) = det(Sigma(:,:,j)) ;
            end
            tau = 1./((sqrt(2*pi)^d)*DetSigma) .* exp(-.5*sm) .* alpha ;
            tau_old = tau./(sum(tau,2)) ;
            tau = tau_old.^(1/T) ; tau = tau./(sum(tau,2)) ;
            
            T = vecT(iter) ;

            cp2 = cumsum(tau,2) ; cp1 = [zeros(n,1) , cp2(:,1:(end-1))] ;
            u = rand(n,1) ;
            z = (cp1<=u)&(u<cp2) ; c = sum(z) ;

            S1 = S1 + gamma(iter)*(c-S1) ;
            S2 = S2 + gamma(iter)*(Y'*z-S2) ;
            for j = 1:m
                Yj = Y.*z(:,j) ;
                S3(:,:,j) = S3(:,:,j) + gamma(iter)*(Yj'*Yj-S3(:,:,j)) ;
            end

            alpha = S1/n ;%+ 000.1*ones(1,m) ; alpha = alpha/sum(alpha) ;
            mu = S2./S1 ; 
            for j = 1:m
                Sigma(:,:,j) = S3(:,:,j)-S2(:,j)*mu(:,j)' ;
                Sigma(:,:,j) = Sigma(:,:,j)/S1(j) ;%+ 1e-3*eye(d) ;
            end

        end

        if iter >= maxIter
            disp(' ')
            error('tmp-SAEM::need more iterations')
        else
            strg = ['tmp-SAEM - Cvgce takes ',num2str(iter),' iters'] ;
            % disp(' ')
            % disp(['tempering-SAEM algorithm : Convergence takes ',num2str(iter),' iterations.'])
            % disp(' ')
        end
    end
    opt.VANILLA_tmpSAEM = @VANILLA_tempering_SAEM_GMM ;
 
        
  % ==================================================================== %
      
    % PLOT tmpSAEM

                                                     
    function [alpha,mu,Sigma,iter,strg,vecT] = PLOT_tempering_SAEM_GMM (alpha0,mu0,Sigma0,Nburnin,maxIter,pres)
        % Y = vector of observations
        % mu0 = vector of initial means
        % Sigma0 = vector of the initial covariance matrices
        % alpha0 = vector of the initial proposals
        % Nburnin = burn-in period
        % maxIter = limit nb of iterations
        % pres = precision

        m = length(alpha0) ;  % nb clusters
        [n,d] = size(Y) ;     % nb,dim observations

        mu = mu0 ; mu_old = 10*mu ;
        Sigma = Sigma0 ;
        alpha = alpha0 ;

        t = 1:maxIter ; tt = (t+tmp.c*tmp.r)/tmp.r ;
        vecT = 1+ tmp.a.^tt + tmp.b*sin(tt)./tt ;
        T = vecT(1) ;

        gamma = [ones(1,Nburnin),1./(1:maxIter-Nburnin).^(.66)] ;
        S1 = zeros(1,m) ;  S2 = zeros(d,m) ;  S3 = zeros(d,d,m) ;

        sm = zeros(n,m) ;  DetSigma = zeros(1,m) ;

        figure(7) ; clf ; hold on

        iter = 0 ;
        while (norm(mu-mu_old)>pres) && (iter<maxIter)
            iter = iter+1 ;
            mu_old = mu ;

            for j = 1:m
                Ym = Y-mu(:,j)' ;
                sm(:,j) = sum(Ym/Sigma(:,:,j) .* Ym, 2) ;
                DetSigma(j) = det(Sigma(:,:,j)) ;
            end
            tau = 1./((sqrt(2*pi)^d)*DetSigma) .* exp(-.5*sm) .* alpha ;
            tau_old = tau./(sum(tau,2)) ;
            tau = tau_old.^(1/T) ; 

            T = vecT(iter) ;

            cp2 = cumsum(tau,2) ;  cp1 = [zeros(n,1) , cp2(:,1:(end-1))] ;
            u = cp2(end)*rand(n,1) ;
            z = (cp1<=u)&(u<cp2) ;  c = sum(z) ;

            S1 = S1 + gamma(iter)*(c-S1) ;
            S2 = S2 + gamma(iter)*(Y'*z-S2) ;
            for j = 1:m
                Yj = Y.*z(:,j) ;
                S3(:,:,j) = S3(:,:,j) + gamma(iter)*(Yj'*Yj-S3(:,:,j)) ;
            end

            alpha = S1/n + 0.01*ones(1,m) ;  
            mu = S2./S1 ; 
            for j = 1:m
                Sigma(:,:,j) = S3(:,:,j)-S2(:,j)*mu(:,j)' ;
                Sigma(:,:,j) = Sigma(:,:,j)/S1(j) ;
            end

             if iter<=20
                figure(7) ; subplot(10,2,iter) ; hold on
                cmap = colormap(lines(5)) ; cmap = cmap([2,4,5],:) ;
                colormap(cmap) ;
                scatter(Y(:,1),Y(:,2),[],z*[1,2,3]')
                for j = 1:m
                    plot(mu(1,j), mu(2,j), '*', 'markersize', 10, 'linewidth',2,'color',cmap(j,:)) ;
                    plot(mu(1,j), mu(2,j), 'k*', 'markersize', 5, 'linewidth',1) ;
                end
                title(['iteration : ',num2str(iter)])
            end

        end

        if iter >= maxIter
            disp(' ')
            error('tmp-SAEM::need more iterations')
        else
            strg = ['tmp-SAEM - Cvgce takes ',num2str(iter),' iters'] ;
            % disp(' ')
            % disp(['tempering-SAEM algorithm : Convergence takes ',num2str(iter),' iterations.'])
            % disp(' ')
        end
    end
    opt.PLOT_tmpSAEM = @PLOT_tempering_SAEM_GMM ;
        
        
  % ==================================================================== %
        
    % VEC tmpSAEM

                        
    function [alpha,mu,Sigma,iter,vec_mu,vec_z,vec_tau,vec_Sigma] = VEC_tempering_SAEM_GMM (alpha0,mu0,Sigma0,Nburnin,maxIter,pres)
        % Y = vector of observations
        % mu0 = vector of initial means
        % Sigma0 = vector of the initial covariance matrices
        % alpha0 = vector of the initial proposals
        % Nburnin = burn-in period
        % maxIter = limit nb of iterations
        % pres = precision

        m = length(alpha0) ;  % nb clusters
        [n,d] = size(Y) ;     % nb,dim observations

        mu = mu0 ; mu_old = 10*mu ;
        Sigma = Sigma0 ;
        alpha = alpha0 ;

        t = 1:maxIter ; tt = (t+tmp.c*tmp.r)/tmp.r ;  %(tmp.T0-tmp.b-1)*tmp.a.^(t/tmp.r) + 
        vecT = 1+ tmp.a.^tt + tmp.b*sin(tt)./tt ;
        T = vecT(1) ;

        gamma = [ones(1,Nburnin),1./(1:maxIter-Nburnin).^(.66)] ;
        S1 = zeros(1,m) ;  S2 = zeros(d,m) ;  S3 = zeros(d,d,m) ;

        sm = zeros(n,m) ;  DetSigma = zeros(1,m) ;

        vec_mu = zeros(d,m,maxIter+1) ; vec_mu(:,:,1) = mu0 ;
        vec_z = zeros(n,m,maxIter) ;
        vec_tau = zeros(n,m,maxIter) ;
        vec_Sigma = zeros(d,d,m,maxIter) ;

        iter = 0 ;
        while (norm(mu-mu_old)>pres) && (iter<maxIter)
            iter = iter+1 ;
            mu_old = mu ;

            for j = 1:m
                Ym = Y-mu(:,j)' ;
                sm(:,j) = sum(Ym/Sigma(:,:,j) .* Ym, 2) ;
                DetSigma(j) = det(Sigma(:,:,j)) ;
            end
            tau = 1./((sqrt(2*pi)^d)*DetSigma) .* exp(-.5*sm) .* alpha ;
            tau_old = tau./(sum(tau,2)) ;
            tau = tau_old.^(1/T) ;

            T = vecT(iter) ;

            cp2 = cumsum(tau,2) ;  cp1 = [zeros(n,1) , cp2(:,1:(end-1))] ;
            u = cp2(end)*rand(n,1) ;
            z = (cp1<=u)&(u<cp2) ;  c = sum(z) ;

            S1 = S1 + gamma(iter)*(c-S1) ;
            S2 = S2 + gamma(iter)*(Y'*z-S2) ;
            for j = 1:m
                Yj = Y.*z(:,j) ;
                S3(:,:,j) = S3(:,:,j) + gamma(iter)*(Yj'*Yj-S3(:,:,j)) ;
            end

            alpha = S1/n + 0.001*ones(1,m)/m ;  
            mu = S2./S1 ; 
            for j = 1:m
                Sigma(:,:,j) = S3(:,:,j)-S2(:,j)*mu(:,j)' ;
                Sigma(:,:,j) = Sigma(:,:,j)/S1(j) ;
            end

            vec_mu(:,:,iter+1) = mu ;  vec_Sigma(:,:,:,iter) = Sigma ;
            vec_z(:,:,iter) = z ;  vec_tau(:,:,iter) = tau ;
        end

        if iter >= maxIter
            disp(' ')
            error('tmp-SAEM::need more iterations')
        else
            strg = ['tmp-SAEM - Cvgce takes ',num2str(iter),' iters'] ;
            % disp(' ')
            % disp(['tempering-SAEM algorithm : Convergence takes ',num2str(iter),' iterations.'])
            % disp(' ')
        end
    end
    opt.VEC_tmpSAEM = @VEC_tempering_SAEM_GMM ;b      n     
        
        
end
    
