
%% Load data

% Y = dataset, each line represents a sample
% Y is of size [n,d]


clear ; load('Data/******.mat')


%% Initialization

[n,d] = size(Y) ;

mu0 = ones(2,3).*mean(Y)' ;
Sigma0 = zeros([d,d,m]) ;
for j = 1:m
    Sigma0(:,:,j) = cov(Y) ;
end
alpha0 = (1/m)*ones(1,m) ; 


% ==================== % ==================== % ==================== %

%% EM algorithm
 
maxIter = 5e3 ;
pres = 1e-4 ;

t_EM = -cputime ;
[alpha_EM,mu_EM,Sigma_EM,cvgce_EM] = EM_GMM(Y,alpha0,mu0,Sigma0,maxIter,pres) ;
t_EM = t_EM + cputime ;


% ==================== % ==================== % ==================== %

%% SAEM algorithm

alt_SAEM = 1 ;  % {1:vanilla, 2:plot, 3:vec}
clear SAEM_GMM ; opt = SAEM_GMM(Y,alt_SAEM) ;

maxIter = 5e3 ; Nburnin = 500 ;
pres = 1e-4 ;

% Results of the SAEM
if alt_SAEM == 3
    t_SAEM = -cputime ;
    [alpha_SAEM,mu_SAEM,Sigma_SAEM,cvgce_SAEM,vec_mu_SAEM,vec_z_SAEM,vec_tau_SAEM,vec_Sigma_SAEM] = ...
        opt.SAEM(alpha0,mu0,Sigma0,Nburnin,maxIter,pres) ;
    t_SAEM = t_SAEM + cputime ;
else
    fprintf(' %1.0u',cpt);
    t_SAEM = -cputime ;
    [alpha_SAEM,mu_SAEM,Sigma_SAEM,cvgce_SAEM] = opt.SAEM(alpha0,mu0,Sigma0,Nburnin,maxIter,pres) ;
    t_SAEM = t_SAEM + cputime ;
end


% ==================== % ==================== % ==================== %

%% tempering-SAEM algorithm

alt_tmpSAEM = 1 ; % {1:vanilla, 2:plot, 3:vec}
clear tmpSAEM_GMM ; opt = tmpSAEM_GMM(Y,alt_tmpSAEM) ;

maxIter = 5e3 ; Nburnin = 500 ;
pres = 1e-4 ;

tmp = [] ; tmp.a = 0 ;
tmp.b = -1 ; tmp.r = 1 ; tmp.c = 1 ;

% Results of the tempering SAEM
if alt_tmpSAEM == 3
    t_tmpSAEM = -cputime ;
    [alpha_tmpSAEM,mu_tmpSAEM,Sigma_tmpSAEM,cvgce_tmpSAEM,vec_mu_tmpSAEM,vec_z_tmpSAEM,vec_tau_tmpSAEM,vec_Sigma_tmpSAEM] = ...
        opt.tmpSAEM(alpha0,mu0,Sigma0,tmp,Nburnin,maxIter,pres) ;
    t_tmpSAEM = t_tmpSAEM + cputime ;
else
    t_tmpSAEM = -cputime ;
    [alpha_tmpSAEM,mu_tmpSAEM,Sigma_tmpSAEM,cvgce_tmpSAEM] = ...
        opt.tmpSAEM(alpha0,mu0,Sigma0,tmp,Nburnin,maxIter,pres) ;
    t_tmpSAEM = t_tmpSAEM + cputime ;
end

