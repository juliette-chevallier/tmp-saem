function plt = plot_GMM(x1,x2,y1,y2,sx,sy)

if nargin < 1
    x1 = -11 ;
    x2 = 11 ;
    y1 = -6 ;
    y2 = 6 ;
end

if nargin < 6
    sx = 7*(x2-x1) ; sy = 7*(y2-y1) ;
end


function plt_traj = plot_traj(Y,Nrun)

[n,d] = size(Y) ;

    function plot_mean(vec_mu,vec_z,vec_cvgce)
        m = size(vec_z(1,:,1,1),2) ;

        t_max = max(vec_cvgce) ; 
        nb = zeros(1,t_max+1) ;
        for cpt=1:Nrun
            for iter=1:t_max+1
                if vec_mu(:,:,iter,cpt)~=0
                    nb(iter) = nb(iter)+1 ;
                end
            end
        end

        vec_mu = sum(vec_mu(:,:,1:t_max+1,:),4) ;
        vec_z_tmp = sum(vec_z(:,:,1:t_max,:),4) ;

        for iter = 1:t_max+1
            vec_mu(:,:,iter) = ...
                vec_mu(:,:,iter)/nb(iter) ;
        end

        vec_z = zeros([n,m,t_max]) ;
        for iter = 1:t_max
            for i = 1:n
                mx = max(vec_z_tmp(i,:,iter)) ;
                c = find(vec_z_tmp(i,:,iter) == mx) ;
                if length(c) > 1
                    t = (rand<.5)+1 ; c=c(t) ;
                end
                vec_z(i,c,iter) = 1 ;
            end
        end

        figure(2) ; clf ;
        cmap = colormap(lines(5)) ; cmap = cmap([2,4,5],:) ;
        old_mu_x = zeros(24,m) ; old_mu_y = zeros(24,m) ;
        for iter = 1:24
            subplot(6,4,iter) ; hold on ; colormap(cmap) ;
            scatter(Y(:,1),Y(:,2),[],vec_z(:,:,iter)*[1,2,3]')
            for j = 1:m
                old_mu_x(iter,j) = vec_mu(1,j,iter) ; old_mu_y(iter,j) = vec_mu(2,j,iter) ;
                plot(old_mu_x(1:iter,j),old_mu_y(1:iter,j),':+','linewidth',1,'markersize',1,'color',cmap(j,:))
                plot(vec_mu(1,j,iter),vec_mu(2,j,iter),'*','markersize',10,'linewidth',2,'color',cmap(j,:)) ;
                plot(vec_mu(1,j,iter),vec_mu(2,j,iter),'k*','markersize',5,'linewidth',1) ;
            end
            title(['Nb runs : ',num2str(nb(iter))])
        end
    end
    plt_traj.plt_mean = @plot_mean ;


    function plot_1run(vec_mu,vec_z,t_cvgce)
        m = size(vec_z(1,:,1,1),2) ;

        vec_mu = vec_mu(:,:,1:t_cvgce+1,:) ;
        vec_z = vec_z(:,:,1:t_cvgce,:) ;

        figure(2) ; clf ;
        cmap = colormap(lines(5)) ; cmap = cmap([2,4,5],:) ;
        old_mu_x = zeros(24,m) ; old_mu_y = zeros(24,m) ;
        for iter = 1:24
            subplot(6,4,iter) ; hold on ; colormap(cmap) ;
            scatter(Y(:,1),Y(:,2),1,vec_z(:,:,iter)*[1,2,3]','x')
            for j = 1:m
                old_mu_x(iter,j) = vec_mu(1,j,iter) ; old_mu_y(iter,j) = vec_mu(2,j,iter) ;
                plot(old_mu_x(1:iter,j),old_mu_y(1:iter,j),':+','linewidth',1,'markersize',1,'color',cmap(j,:))
                plot(vec_mu(1,j,iter),vec_mu(2,j,iter),'*','markersize',10,'linewidth',2,'color',cmap(j,:)) ;
                plot(vec_mu(1,j,iter),vec_mu(2,j,iter),'k*','markersize',5,'linewidth',1) ;
            end
        end
    end
    plt_traj.plt_1run = @plot_1run ;


    function plot_1run_clases(vec_mu,vec_tau,vec_Sigma,t_cvgce,l)
        m = size(vec_tau(1,:,1,1),2) ;

        vec_mu = vec_mu(:,:,1:t_cvgce+1,:) ;
        vec_tau = vec_tau(:,:,1:t_cvgce+1,:) ;
        vec_Sigma = vec_Sigma(:,:,:,1:t_cvgce+1,:) ;

        x = linspace(x1,x2,sx) ;
        y = linspace(y1,y2,sy) ;
        [xx,yy] = meshgrid(x,y) ;
        xx = [xx(:),yy(:)] ;

        figure(l) ; clf ; nb = 14 ;
        old_mu_x = zeros(24,m) ; old_mu_y = zeros(24,m) ;
        for iter = 1:nb
            for j = 1:m
                subplot(nb,m,(iter-1)*m+j) ; hold on
%                 colormap(parula) ; 
%                 colorbar ; 
                caxis([-0.12 1.12])
                scatter(Y(:,1),Y(:,2),1,vec_tau(:,j,iter),'x')
                old_mu_x(iter,j) = vec_mu(1,j,iter) ; old_mu_y(iter,j) = vec_mu(2,j,iter) ;
                plot(old_mu_x(1:iter,j),old_mu_y(1:iter,j),'k:+','linewidth',1,'markersize',1)
                plot(vec_mu(1,j,iter),vec_mu(2,j,iter),'k*','markersize',10,'linewidth',2) ;
                title(['iter = ',num2str(iter),'  ;  m = ',num2str(j)])

                tmp = xx-vec_mu(:,j,iter)' ;
                Z = 1/sqrt((2*pi)^d*det(vec_Sigma(:,:,j,iter))) ...
                    * exp(-1/2 *sum((tmp/vec_Sigma(:,:,j,iter).*tmp),2)) ;
                Z = reshape(Z,sy,sx) ;
                contour(x,y,Z,'color','k','LineStyle','-') ;
            end
        end
    end
    plt_traj.plt_1run_clases = @plot_1run_clases ;
end
plt.plt_traj = @plot_traj ;


% ==================== % ==================== % ==================== %


function plot_3classes(Y,theta_true,mu0,mu_EM,mu_SAEM,mu_tmpSAEM,...
    Sigma0,Sigma_EM,Sigma_SAEM,Sigma_tmpSAEM)

    if nargin <8
        plt = 0 ;
    else
        Sigma_SAEM = mean(Sigma_SAEM,4) ;
        Sigma_tmpSAEM = mean(Sigma_tmpSAEM,4) ;
        plt = 1 ;
    end

    [d,m] = size(mu0) ;

    mu_true = theta_true{2} ;
    Sigma_true = theta_true{3} ;


    figure(1) ; clf ; hold on
    cmap = colormap(lines) ;

    % Centroids
    plot(mu_true(1,:), mu_true(2,:), 'kx', 'markersize', 12, 'linewidth',2) ;

    plot(mu0(1,:), mu0(2,:), 'x', 'markersize', 20, 'linewidth',2,...
        'color',cmap(1,:)) ;

    plot(mu_EM(1,:), mu_EM(2,:), '*', 'markersize', 10, 'linewidth',2,...
        'color',cmap(2,:)) ;

    mu_SAEM = mean(mu_SAEM,3) ;
    plot(mu_SAEM(1,:), mu_SAEM(2,:), '*', 'markersize', 10, 'linewidth',2,...
        'color',cmap(3,:)) ;

    mu_tmpSAEM = mean(mu_tmpSAEM,3) ;
    plot(mu_tmpSAEM(1,:), mu_tmpSAEM(2,:), '*', 'markersize', 10, 'linewidth',2,...
        'color',cmap(4,:)) ;


    % Gaussian
    x = linspace(x1,x2,sx) ;
    y = linspace(y1,y2,sy) ;
    [xx,yy] = meshgrid(x,y) ;
    xx = [xx(:),yy(:)] ;

    for j = 1:m
        tmp = xx-mu_true(:,j)' ;
        Z = 1/sqrt((2*pi)^d*det(Sigma_true(:,:,j))) ...
            * exp(-1/2 *sum((tmp/Sigma_true(:,:,j).*tmp),2)) ;
        Z = reshape(Z,sy,sx) ;
        contour(x,y,Z,'color','k','LineStyle',':') ;
    end    

    scatter(Y(:,1),Y(:,2),'.')

    if plt
        for j = 1:m
            tmp = xx-mu0(:,j)' ;
            Z = 1/sqrt((2*pi)^d*det(Sigma0(:,:,j))) ...
                * exp(-1/2 *sum((tmp/Sigma0(:,:,j).*tmp),2)) ;
            Z = reshape(Z,sy,sx) ;
            contour(x,y,Z,'color',cmap(1,:),'LineStyle',':') ;
        end

        for j = 1:m
            tmp = xx-mu_EM(:,j)' ;
            Z = 1/sqrt((2*pi)^d*det(Sigma_EM(:,:,j))) ...
                * exp(-1/2 *sum((tmp/Sigma_EM(:,:,j).*tmp),2)) ;
            Z = reshape(Z,sy,sx) ;
            contour(x,y,Z,'color',cmap(2,:),'LineStyle',':') ;
        end

        for j = 1:m
            tmp = xx-mu_SAEM(:,j)' ;
            Z = 1/sqrt((2*pi)^d*det(Sigma_SAEM(:,:,j))) ...
                * exp(-1/2 *sum((tmp/Sigma_SAEM(:,:,j).*tmp),2)) ;
            Z = reshape(Z,sy,sx) ;
            contour(x,y,Z,'color',cmap(3,:),'LineStyle','-.') ;
        end

        for j = 1:m
            tmp = xx-mu_tmpSAEM(:,j)' ;
            Z = 1/sqrt((2*pi)^d*det(Sigma_tmpSAEM(:,:,j))) ...
                * exp(-1/2 *sum((tmp/Sigma_tmpSAEM(:,:,j).*tmp),2)) ;
            Z = reshape(Z,sy,sx) ;
            contour(x,y,Z,'color',cmap(4,:),'LineStyle','-') ;
        end
    end

    axis([x1 x2 y1 y2]) ; legend('true','init','EM','SAEM','tmpSAEM')
end
plt.plt_3cl = @plot_3classes ;


end