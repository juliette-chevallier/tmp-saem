# tempering-SAEM

(Matlab) Codes associated to the tempering-SAEM algorithm
See Script.m for an example

--

A new class of EM algorithms. Escaping local maxima and handling intractable sampling. Stéphanie Allassonnière et Juliette Chevallier. 2020.