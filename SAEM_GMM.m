function opt = SAEM_GMM (Y,alt)

    % alt = 1  - vanilla SAEM
    % alt = 2  - plot SAEM
    % alt = 3  - vec SAEM

    if nargin<2
        alt = 1 ;
    end

    if alt==1
        opt.SAEM = @VANILLA_SAEM_GMM ;
    elseif alt == 2
        opt.SAEM = @PLOT_SAEM_GMM ;
    else
        opt.SAEM = @VEC_SAEM_GMM ;
    end
    
    
    function [alpha,mu,Sigma,iter,strg,tau] = VANILLA_SAEM_GMM (alpha0,mu0,Sigma0,Nburnin,maxIter,pres)
        % Y = vector of observations
        % mu0 = vector of initial means
        % Sigma0 = vector of the initial covariance matrices
        % alpha0 = vector of the initial proposals
        % Nburnin = burn-in period
        % maxIter = limit nb of iterations
        % pres = precision

        m = length(alpha0) ;  % nb clusters
        [n,d] = size(Y) ;     % nb,dim observations

        mu = mu0 ; mu_old = 10*mu ;
        Sigma = Sigma0 ;
        alpha = alpha0 ;

        gamma = [ones(1,Nburnin),1./(1:maxIter-Nburnin).^(.66)] ;
        S1 = zeros(1,m) ;
        S2 = zeros(d,m) ;
        S3 = zeros(d,d,m) ;

        sm = zeros(n,m) ;
        DetSigma = zeros(1,m) ;

        iter = 0 ;
        while (norm(mu-mu_old)>pres) && (iter<maxIter)
            iter = iter+1 ;
            mu_old = mu ;

            for j = 1:m
                Ym = Y-mu(:,j)' ;
                sm(:,j) = sum(Ym/Sigma(:,:,j) .* Ym, 2) ;
                DetSigma(j) = det(Sigma(:,:,j)) ;
            end
            tau = 1./((sqrt(2*pi)^d)*DetSigma) .* exp(-.5*sm) .* alpha ;
            tau = tau./(sum(tau,2)) ;

            cp2 = cumsum(tau,2) ;
            cp1 = [zeros(n,1) , cp2(:,1:(end-1))] ;
            u = rand(n,1) ;
            z = (cp1<=u)&(u<cp2) ;
            c = sum(z) ;

            S1 = S1 + gamma(iter)*(c-S1) ;
            S2 = S2 + gamma(iter)*(Y'*z-S2) ;
            for j = 1:m
                Yj = Y.*z(:,j) ;
                S3(:,:,j) = S3(:,:,j) + gamma(iter)*(Yj'*Yj-S3(:,:,j)) ;
            end

            alpha = S1/n ;
            mu = S2./S1 ;
            for j = 1:m
                Sigma(:,:,j) = S3(:,:,j)-S2(:,j)*mu(:,j)' ;
                Sigma(:,:,j) = Sigma(:,:,j)/S1(j) ;
            end
        end

        if iter >= maxIter
            disp(' ')
            error('SAEM::need more iterations')
        else
            strg = ['SAEM - Cvgce takes ',num2str(iter),' iters'] ;
%             disp(' ')
%             disp(['SAEM algorithm : Convergence takes ',num2str(iter),' iterations.'])
%             disp(' ')
        end
    end
    opt.VANILLA_SAEM = @VANILLA_SAEM_GMM ;
       
        
  % ==================================================================== %
        
    % PLOT SAEM

    
    function [alpha,mu,Sigma,strg] = PLOT_SAEM_GMM (alpha0,mu0,Sigma0,Nburnin,maxIter,pres)
        % Y = vector of observations
        % mu0 = vector of initial means
        % Sigma0 = vector of the initial covariance matrices
        % alpha0 = vector of the initial proposals
        % Nburnin = burn-in period
        % maxIter = limit nb of iterations
        % pres = precision
                                                     
        m = length(alpha0) ;  % nb clusters
        [n,d] = size(Y) ;     % nb,dim observations

        mu = mu0 ; mu_old = 10*mu ;
        Sigma = Sigma0 ;
        alpha = alpha0 ;

        gamma = [ones(1,Nburnin),1./(1:maxIter-Nburnin).^(.66)] ;
        S1 = zeros(1,m) ;
        S2 = zeros(d,m) ;
        S3 = zeros(d,d,m) ;

        sm = zeros(n,m) ;
        DetSigma = zeros(1,m) ;

        figure(8) ; clf ; hold on

        iter = 0 ;
        while (norm(mu-mu_old)>pres) && (iter<maxIter)
            iter = iter+1 ;
            mu_old = mu ;

            for j = 1:m
                Ym = Y-mu(:,j)' ;
                sm(:,j) = sum(Ym/Sigma(:,:,j) .* Ym, 2) ;
                DetSigma(j) = det(Sigma(:,:,j)) ;
            end
            tau = 1./((sqrt(2*pi)^d)*DetSigma) .* exp(-.5*sm) .* alpha ;
            tau = tau./(sum(tau,2)) ;

            cp2 = cumsum(tau,2) ;
            cp1 = [zeros(n,1) , cp2(:,1:(end-1))] ;
            u = rand(n,1) ;
            z = (cp1<=u)&(u<cp2) ;
            c = sum(z) ;

            S1 = S1 + gamma(iter)*(c-S1) ;
            S2 = S2 + gamma(iter)*(Y'*z-S2) ;
            for j = 1:m
                Yj = Y.*z(:,j) ;
                S3(:,:,j) = S3(:,:,j) + gamma(iter)*(Yj'*Yj-S3(:,:,j)) ;
            end

            alpha = S1/n ;
            mu = S2./S1 ;
            for j = 1:m
                Sigma(:,:,j) = S3(:,:,j)-S2(:,j)*mu(:,j)' ;
                Sigma(:,:,j) = Sigma(:,:,j)/S1(j) ;
            end

            if iter<=30
                figure(8) ; subplot(15,2,iter) ; hold on
                cmap = colormap(lines(5)) ; cmap = cmap([2,4,5],:) ;
                colormap(cmap) ;
                scatter(Y(:,1),Y(:,2),[],z*[1,2,3]')
                for j = 1:m
                    plot(mu(1,j), mu(2,j), '*', 'markersize', 10, 'linewidth',2,'color',cmap(j,:)) ;
                    plot(mu(1,j), mu(2,j), 'k*', 'markersize', 5, 'linewidth',1) ;
                end
                title(['iteration : ',num2str(iter)])
            end

        end

        if iter >= maxIter
            disp(' ')
            error('SAEM::need more iterations')
        else
            strg = ['SAEM - Cvgce takes ',num2str(iter),' iters'] ;
%             disp(' ')
%             disp(['SAEM algorithm : Convergence takes ',num2str(iter),' iterations.'])
%             disp(' ')
        end
    end
    opt.PLOT_SAEM = @PLOT_SAEM_GMM ;
     
        
  % ==================================================================== %
        
    % VEC SAEM

    
    function [alpha,mu,Sigma,iter,vec_mu,vec_z,vec_tau,vec_Sigma] = ...
            VEC_SAEM_GMM (alpha0,mu0,Sigma0,Nburnin,maxIter,pres)
        % Y = vector of observations
        % mu0 = vector of initial means
        % Sigma0 = vector of the initial covariance matrices
        % alpha0 = vector of the initial proposals
        % Nburnin = burn-in period
        % maxIter = limit nb of iterations
        % pres = precision

        m = length(alpha0) ;  % nb clusters
        [n,d] = size(Y) ;     % nb,dim observations

        mu = mu0 ; mu_old = 10*mu ;
        Sigma = Sigma0 ;
        alpha = alpha0 ;

        gamma = [ones(1,Nburnin),1./(1:maxIter-Nburnin).^(.66)] ;
        S1 = zeros(1,m) ;
        S2 = zeros(d,m) ;
        S3 = zeros(d,d,m) ;

        sm = zeros(n,m) ;
        DetSigma = zeros(1,m) ;
        
        vec_mu = zeros(d,m,maxIter+1) ; vec_mu(:,:,1) = mu0 ;
        vec_z = zeros(n,m,maxIter) ;
        vec_tau = zeros(n,m,maxIter+1) ; vec_tau(:,:,1) = ones(n,m)/m ;
        vec_Sigma = zeros(d,d,m,maxIter+1) ; vec_Sigma(:,:,:,1) = Sigma0 ;

        iter = 0 ;
        while (norm(mu-mu_old)>pres) && (iter<maxIter)
            iter = iter+1 ;
            mu_old = mu ;

            for j = 1:m
                Ym = Y-mu(:,j)' ;
                sm(:,j) = sum(Ym/Sigma(:,:,j) .* Ym, 2) ;
                DetSigma(j) = det(Sigma(:,:,j)) ;
            end
            tau = 1./((sqrt(2*pi)^d)*DetSigma) .* exp(-.5*sm) .* alpha ;
            tau = tau./(sum(tau,2)) ;

            cp2 = cumsum(tau,2) ;
            cp1 = [zeros(n,1) , cp2(:,1:(end-1))] ;
            u = rand(n,1) ;
            z = (cp1<=u)&(u<cp2) ;
            c = sum(z) ;

            S1 = S1 + gamma(iter)*(c-S1) ;
            S2 = S2 + gamma(iter)*(Y'*z-S2) ;
            for j = 1:m
                Yj = Y.*z(:,j) ;
                S3(:,:,j) = S3(:,:,j) + gamma(iter)*(Yj'*Yj-S3(:,:,j)) ;
            end

            alpha = S1/n ;
            mu = S2./S1 ;
            for j = 1:m
                Sigma(:,:,j) = S3(:,:,j)-S2(:,j)*mu(:,j)' ;
                Sigma(:,:,j) = Sigma(:,:,j)/S1(j) ;
            end
            
            vec_mu(:,:,iter+1) = mu ;
            vec_z(:,:,iter) = z ;
            vec_tau(:,:,iter+1) = tau ;
            vec_Sigma(:,:,:,iter+1) = Sigma ;
        end

        if iter >= maxIter
            disp(' ')
            error('SAEM::need more iterations')
        else
            strg = ['SAEM - Cvgce takes ',num2str(iter),' iters'] ;
%             disp(' ')
%             disp(['SAEM algorithm : Convergence takes ',num2str(iter),' iterations.'])
%             disp(' ')
        end
    end
    opt.VEC_SAEM = @VEC_SAEM_GMM ;
    
end
